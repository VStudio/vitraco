<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TCategorie;
use AppBundle\Entity\TProduit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller
{
    /**
     * Search for sponsor info
     * @Route("/loadMoreBikes", name="loadMoreBikes", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function loadMoreBikesAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $page = $request->get('page');
            $page = (int)$page;
//        Check if categorie exist

            $em = $this->getDoctrine()->getManager();
            $nbMaxParPage = 5;

            $singleCat = $em->getRepository(TProduit::class)->getMotoOffers2($page, $nbMaxParPage);

            return $this->container->get('templating')->renderResponse('default/ajaxviews/loadmoto.html.twig', array(
                'products' => $singleCat
            ));
        }
    }
    /**
     * Search for sponsor info
     * @Route("/loadMorePiece", name="loadMorePiece", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function loadMorePieceAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $page = $request->get('page');
            $page = (int)$page;
//        Check if categorie exist

            $em = $this->getDoctrine()->getManager();
            $nbMaxParPage = 5;

            $singleCat = $em->getRepository(TProduit::class)->getPieceOffers2($page, $nbMaxParPage);

            return $this->container->get('templating')->renderResponse('default/ajaxviews/loadproduit.html.twig', array(
                'singleCat' => $singleCat
            ));
        }
    }

    /**
     * Search for sponsor info
     * @Route("/loadMoreBikes2", name="loadMoreBikes2", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function loadMoreBikes2Action(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $page = $request->get('page');
            $page = (int)$page;
//        Check if categorie exist
            $libelle = $request->get('libelle');
            $category = $em->getRepository(TCategorie::class)->findOneBy(array(
                'libelle' => $libelle,
            ));
            $em = $this->getDoctrine()->getManager();
            $nbMaxParPage = 5;

            $singleCat = $em->getRepository(TProduit::class)->getLastestOffer3($page, $nbMaxParPage, $category->getId());
//dump($singleCat);die();

            return $this->container->get('templating')->renderResponse('default/ajaxviews/loadmoto.html.twig', array(
                'products' => $singleCat
            ));
        }
    }

    /**
     * Search for sponsor info
     * @Route("/loadMoreMewBikes", name="loadMoreMewBikes", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function loadMoreMewBikesAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $page = $request->get('page');
            $page = (int)$page;
//        Check if categorie exist
            $em = $this->getDoctrine()->getManager();
            $nbMaxParPage = 5;

            $singleCat = $em->getRepository(TProduit::class)->getLastestOffer2($page, $nbMaxParPage);

            return $this->container->get('templating')->renderResponse('default/ajaxviews/loadmoto.html.twig', array(
                'products' => $singleCat
            ));
        }
    }
    /**
     * Search for sponsor info
     * @Route("/loadMoreMsg", name="loadMoreMsg", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function loadMoreMsgAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $page = $request->get('page');
            $page = (int)$page;
//        Check if categorie exist
            $libelle = $request->get('libelle');
            $category = $em->getRepository(TCategorie::class)->findOneBy(array(
                'libelle' => $libelle,
            ));
            $em = $this->getDoctrine()->getManager();
            $nbMaxParPage = 5;

            $singleCat = $em->getRepository(TProduit::class)->getcaegorizedOffers1($page, $nbMaxParPage, $category->getId());
//dump($singleCat);die();

            return $this->container->get('templating')->renderResponse('default/ajaxviews/loadproduit.html.twig', array(
                'singleCat' => $singleCat
            ));
        }
    }

    /**
     * Search for lubrication
     * @Route("/loadMoreLub", name="loadMoreLub", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function loadMoreLubAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $page = $request->get('page');
            $page = (int)$page;
            $em = $this->getDoctrine()->getManager();
            $nbMaxParPage = 5;

            $singleCat = $em->getRepository(Tproduit::class)->getLubOffers2($page, $nbMaxParPage);


            return $this->container->get('templating')->renderResponse('default/ajaxviews/loadproduit.html.twig', array(
                'singleCat' => $singleCat
            ));
        }
    }
    /**
     * Search for lubrication
     * @Route("/loadMoreLubCat", name="loadMoreLubCat", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function loadMoreLubCatAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $page = $request->get('page');
            $page = (int)$page;
//        Check if categorie exist
            $libelle = $request->get('libelle');
            $category = $em->getRepository(TCategorie::class)->findOneBy(array(
                'libelle' => $libelle,
            ));
            $em = $this->getDoctrine()->getManager();
            $nbMaxParPage = 1;

            $singleCat = $em->getRepository(TProduit::class)->getcaegorizedLub($page, $nbMaxParPage, $category->getId());


            return $this->container->get('templating')->renderResponse('default/ajaxviews/loadproduit.html.twig', array(
                'singleCat' => $singleCat
            ));
        }
    }
}
