<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TCategorie;
use AppBundle\Entity\TDMoto;
use AppBundle\Entity\TMedia;
use AppBundle\Entity\TProduit;
use AppBundle\Entity\TProduitPieces;
use AppBundle\Entity\TPromotions;
use AppBundle\Entity\TTypeProduit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository(Tproduit::class)->findAll();
        $newProducts = $em->getRepository(Tproduit::class)->getLastestOffer();
        return $this->render('default/index.html.twig', array(
            'products' => $products,
            'newProducts' => $newProducts,
        ));
    }

    /**
     * @Route("/slides", name="slides")
     */
    public function slidesAction(Request $request)
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getManager();

        $promotion = $em->getRepository(TPromotions::class)->getActivePromotion();
//         dump($promotion);die();

        return $this->render('default/include/slide.html.twig', array(
            'promotion' => $promotion,
        ));
    }

    /**
     * @Route("/nos_categories_moto", name="nos_categories_moto")
     */
    public function categoiresAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(TCategorie::class)->findBy(array(
            'typeProduit' => 1,
            'estActive' => true
        ));
        return $this->render('default/include/categories.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * @Route("/nos_categories_lubrifiant", name="nos_categories_lubrifiant")
     */
    public function categoireslubrifiantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(TCategorie::class)->findBy(array(
            'typeProduit' => 2,
            'estActive' => true
        ));
        return $this->render('default/include/categoriesLubrifiant.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * @Route("/nos_categories_piece", name="nos_categories_piece")
     */
    public function categoirespieceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(TCategorie::class)->findBy(array(
            'typeProduit' => 3,
            'estActive' => true
        ));
        return $this->render('default/include/categoriesPiece.html.twig', array(
            'categories' => $categories,
        ));
    }


    /**
     * @Route("/categories_moto/{libelle}", name="selectCategorie")
     */
    public function singlecategoiresAction(Request $request)
    {
        $libelle = $request->get('libelle');
//        Check if categorie exist
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(TCategorie::class)->findOneBy(array(
            'libelle' => $libelle,
        ));
        $page = 1;
        $nbMaxParPage = 5;
        if ($category) {
            $singleCat = $em->getRepository(Tproduit::class)->getLastestOffer3($page, $nbMaxParPage, $category->getId());
        } else {
            $singleCat = null;
        }
        return $this->render('default/singlecategory.html.twig', array(
            'products' => $singleCat,
            'category' => $category,
        ));
    }

    /**
     * @Route("/categories_piece/{libelle}/", name="selectCategoriePiece")
     */
    public function singlecategoirespieceAction(Request $request)
    {
        $libelle = $request->get('libelle');
//        Check if categorie exist
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(TCategorie::class)->findOneBy(array(
            'libelle' => $libelle,
        ));

        $page = 1;
        $nbMaxParPage = 5;
        if ($category) {
            $singleCat = $em->getRepository(Tproduit::class)->getcaegorizedOffers1($page, $nbMaxParPage, $category->getId());
        } else {
            $singleCat = null;
        }
//        dump($singleCat);die();
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($singleCat) / $nbMaxParPage),
            'nomRoute' => 'selectCategoriePiece',
            'libelle' => $libelle,
            'paramsRoute' => array()
        );
//        dump($category,$singleCat);die();
        return $this->render('default/singlecategoryPiece.html.twig', array(
            'singleCat' => $singleCat,
            'category' => $category,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/categories_lubrifiants/{libelle}/", name="selectCategorieLubrifiant")
     */
    public function singlecategoireslubAction(Request $request)
    {
        $libelle = $request->get('libelle');
//        Check if categorie exist
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(TCategorie::class)->findOneBy(array(
            'libelle' => $libelle,
        ));
        $page = 1;
        $nbMaxParPage = 5;
        if ($category) {
            $singleCat = $em->getRepository(Tproduit::class)->getcaegorizedLub($page, $nbMaxParPage, $category->getId());
        } else {
            $singleCat = null;
        }
//        dump($singleCat);die();
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($singleCat) / $nbMaxParPage),
            'nomRoute' => 'selectCategoriePiece',
            'libelle' => $libelle,
            'paramsRoute' => array()
        );
//        dump($category,$singleCat);die();
        return $this->render('default/singlecategoryLubrifiant.html.twig', array(
            'singleCat' => $singleCat,
            'category' => $category,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/motos_offers", name="offers")
     */
    public function offersAction(Request $request)
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Tproduit::class)->getMotoOffers();
//        dump($product);die();
        return $this->render('default/include/lesmotot.html.twig', array(
            'offer' => $product,
        ));
    }

    /**
     * @Route("/piece_offers", name="pieceoffers")
     */
    public function pieceoffersAction(Request $request)
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Tproduit::class)->getPieceOffers();
//        dump($product);die();
        return $this->render('default/include/lespiece.html.twig', array(
            'offer' => $product,
        ));
    }

    /**
     * @Route("/lubrifiant_offers", name="lubrifiantoffers")
     */
    public function lubrifiantoffersAction(Request $request)
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Tproduit::class)->getLubOffers();
//        dump($product);die();
        return $this->render('default/include/leslubrifiant.html.twig', array(
            'offer' => $product,
        ));
    }

    /**
     * @Route("/nos_produit", name="products")
     */
    public function productsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $nbMaxParPage = 5;
        $page = 1;
        $product = $em->getRepository(Tproduit::class)->getMotoOffers2($page, $nbMaxParPage);

        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($product) / $nbMaxParPage),
            'nomRoute' => 'products',
            'paramsRoute' => array()
        );
        $typecategory = $em->getRepository(TTypeProduit::class)->find(1);
        $category = $em->getRepository(TCategorie::class)->findOneBy([
            'typeProduit' => $typecategory
        ]);
        return $this->render('default/nosproduit.html.twig', array(
            'products' => $product,
            'pagination' => $pagination,
            'category' => $category
        ));
    }

    /**
     * @Route("/nos_produit_pieces", name="productpieces")
     */
    public function productspiecesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $nbMaxParPage = 5;
        $page = 1;
        $product = $em->getRepository(Tproduit::class)->getPieceOffers2($page, $nbMaxParPage);
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($product) / $nbMaxParPage),
            'nomRoute' => 'productpieces',
            'paramsRoute' => array()
        );
        $typecategory = $em->getRepository(TTypeProduit::class)->find(3);
        $category = $em->getRepository(TCategorie::class)->findOneBy([
            'typeProduit' => $typecategory
        ]);
        return $this->render('default/nosproduitpieces.html.twig', array(
            'singleCat' => $product,
            'pagination' => $pagination,
            'category' => $category
        ));
    }

    /**
     * @Route("/nos_produit_lubrifiants", name="productlubrifiants")
     */
    public function productlubrifiantsAction(Request $request)
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getManager();
        $nbMaxParPage = 5;
        $page = 1;
        $product = $em->getRepository(Tproduit::class)->getLubOffers2($page, $nbMaxParPage);
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($product) / $nbMaxParPage),
            'nomRoute' => 'productlubrifiants',
            'paramsRoute' => array()
        );
        $typecategory = $em->getRepository(TTypeProduit::class)->find(2);
        $category = $em->getRepository(TCategorie::class)->findOneBy([
            'typeProduit' => $typecategory
        ]);
        return $this->render('default/nosproduitlubrifiants.html.twig', array(
            'singleCat' => $product,
            'pagination' => $pagination,
            'category' => $category
        ));
    }

    /**
     * @Route("/page_de_detail_moto/{id}", name="details")
     */
    public function detailsAction(Request $request)
    {
        // replace this example code with whatever you need
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Tproduit::class)->findOneById($id);
        $description = $em->getRepository(TDMoto::class)->findOneBy([
            'produit' => $product
        ]);

        $media = $em->getRepository(TMedia::class)->findBy(array(
            'produit' => $product->getId(),
        ));
        return $this->render('default/details.html.twig', array(
            'product' => $product,
            'medias' => $media,
            'description' => $description,
        ));
    }

    /**
     * @Route("/page_de_detail_piece/{id}", name="detailsP")
     */
    public function detailspieceAction(Request $request)
    {
        // replace this example code with whatever you need
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(TProduitPieces::class)->findOneById($id);
        return $this->render('default/detailsPiece.html.twig', array(
            'product' => $product,
        ));
    }

    /**
     * @Route("/page_de_detail_lubrifiant/{id}", name="detailsL")
     */
    public function detailslubAction(Request $request)
    {
        // replace this example code with whatever you need
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(TProduitPieces::class)->findOneById($id);
        return $this->render('default/detailsLubrifiant.html.twig', array(
            'product' => $product,
        ));
    }

    /**
     * @Route("/les_nouveaute", name="les_nouveaute")
     */
    public function lesnouveauteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $nbMaxParPage = 5;
        $page = 1;
        $new = $em->getRepository(Tproduit::class)->getLastestOffer2($page, $nbMaxParPage);

        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($new) / $nbMaxParPage),
            'nomRoute' => 'les_nouveaute',
            'paramsRoute' => array()
        );

        return $this->render('default/produitnew.html.twig', array(
            'newproducts' => $new,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/contact.html.twig');
    }

    /**
     * @Route("/contact_form", name="contactsent")
     */
    public function contactsentAction(Request $request)
    {
        // replace this example code with whatever you need
        $name = $request->get('name');
        $telephone = $request->get('telephone');
        $email = $request->get('email');
        $content = $request->get('content');

//        Send email
        $this->container->get("app.mailer")->envoiEmail("Demande client", $email, ":default:contactemail.html.twig", array(
            'email' => $email,
            'names' => $name,
            'message' => $content,
            'telephone' => $telephone,
        ));

//        Redirect to homepage
        $session = new Session();
        $message = "Votre message a bien été envoyé, nous vous répondrons dans les plus brefs délais.";
        $session->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('contact');
    }
}
