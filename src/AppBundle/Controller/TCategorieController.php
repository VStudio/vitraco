<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TCategorie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;use Symfony\Component\HttpFoundation\Request;

/**
 * Tcategorie controller.
 *
 * @Route("categorie")
 */
class TCategorieController extends Controller
{
    /**
     * Lists all tCategorie entities.
     *
     * @Route("/", name="categorie_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $tCategories = $em->getRepository('AppBundle:TCategorie')->findAll();
//dump($tCategories);die();
        return $this->render('tcategorie/index.html.twig', array(
            'tCategories' => $tCategories,
        ));
    }

    /**
     * Creates a new tCategorie entity.
     *
     * @Route("/news", name="categorie_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $tCategorie = new Tcategorie();
        $form = $this->createForm('AppBundle\Form\TCategorieType', $tCategorie);
        $form->handleRequest($request);
//dump($tCategorie);die();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $tCategorie->setEstActive(true);
            $em->persist($tCategorie);
            $em->flush();

            return $this->redirectToRoute('categorie_show', array('id' => $tCategorie->getId()));
        }

        return $this->render('tcategorie/new.html.twig', array(
            'tCategorie' => $tCategorie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tCategorie entity.
     *
     * @Route("/{id}", name="categorie_show")
     * @Method("GET")
     */
    public function showAction(TCategorie $tCategorie)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($tCategorie);

        return $this->render('tcategorie/show.html.twig', array(
            'tCategorie' => $tCategorie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tCategorie entity.
     *
     * @Route("/{id}/edit", name="categorie_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TCategorie $tCategorie)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($tCategorie);
        $editForm = $this->createForm('AppBundle\Form\TCategorieType', $tCategorie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categorie_show', array('id' => $tCategorie->getId()));
        }

        return $this->render('tcategorie/edit.html.twig', array(
            'tCategorie' => $tCategorie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tCategorie entity.
     *
     * @Route("/type/{id}", name="personnal_categorie_delete")
     * @Method("DELETE")
     */
    public function PersonaldeleteAction(Request $request, TCategorie $tCategorie)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();

        $tCategories = $em->getRepository('AppBundle:TCategorie')->findOneById($id);

        $em->remove($tCategories);
        $em->flush();
        $tCategories = $em->getRepository('AppBundle:TCategorie')->findAll();

        return $this->render('tcategorie/index.html.twig', array(
            'tCategories' => $tCategories,
        ));

    }

    /**
     * Deletes a tCategorie entity.
     *
     * @Route("/{id}", name="categorie_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TCategorie $tCategorie)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $form = $this->createDeleteForm($tCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tCategorie);
            $em->flush();
        }

        return $this->redirectToRoute('categorie_index');
    }

    /**
     * @Route("/change_produit/", name="categorie_change")
     */
    public function changeStateAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $tCategorie = $em->getRepository(TCategorie::class)->find($id);
//dump($id,$tCategorie);die();
        if ($tCategorie->getEstActive() === true) {
            $tCategorie->setEstActive(false);
            $em->persist($tCategorie);
            $em->flush();
        } else {
            $tCategorie->setEstActive(true);
            $em->persist($tCategorie);
            $em->flush();
        }
        return $this->redirectToRoute('categorie_index');
    }

    /**
     * Creates a form to delete a tCategorie entity.
     *
     * @param TCategorie $tCategorie The tCategorie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TCategorie $tCategorie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorie_delete', array('id' => $tCategorie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
