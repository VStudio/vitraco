<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TMedia;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;use Symfony\Component\HttpFoundation\Request;

/**
 * Tmedia controller.
 *
 * @Route("tmedia")
 */
class TMediaController extends Controller
{
    /**
     * Lists all tMedia entities.
     *
     * @Route("/", name="tmedia_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $tMedia = $em->getRepository('AppBundle:TMedia')->findAll();

        return $this->render('tmedia/index.html.twig', array(
            'tMedia' => $tMedia,
        ));
    }

    /**
     * Creates a new tMedia entity.
     *
     * @Route("/new", name="tmedia_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $tMedia = new Tmedia();
        $form = $this->createForm('AppBundle\Form\TMediaType', $tMedia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tMedia);
            $em->flush();

            return $this->redirectToRoute('tmedia_show', array('id' => $tMedia->getId()));
        }

        return $this->render('tmedia/new.html.twig', array(
            'tMedia' => $tMedia,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tMedia entity.
     *
     * @Route("/{id}", name="tmedia_show")
     * @Method("GET")
     */
    public function showAction(TMedia $tMedia)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($tMedia);

        return $this->render('tmedia/show.html.twig', array(
            'tMedia' => $tMedia,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tMedia entity.
     *
     * @Route("/{id}/edit", name="tmedia_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TMedia $tMedia)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($tMedia);
        $editForm = $this->createForm('AppBundle\Form\TMediaType', $tMedia);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tmedia_edit', array('id' => $tMedia->getId()));
        }

        return $this->render('tmedia/edit.html.twig', array(
            'tMedia' => $tMedia,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tMedia entity.
     *
     * @Route("/{id}", name="tmedia_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TMedia $tMedia)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $form = $this->createDeleteForm($tMedia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tMedia);
            $em->flush();
        }

        return $this->redirectToRoute('tmedia_index');
    }

    /**
     * Creates a form to delete a tMedia entity.
     *
     * @param TMedia $tMedia The tMedia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TMedia $tMedia)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tmedia_delete', array('id' => $tMedia->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
