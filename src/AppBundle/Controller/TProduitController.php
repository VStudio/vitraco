<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TCategorie;
use AppBundle\Entity\TDMoto;
use AppBundle\Entity\TMedia;
use AppBundle\Entity\TProduit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tproduit controller.
 *
     * @Route("produit_moto")
 */
class TProduitController extends Controller
{
    private $targetDir;

    /**
     * Lists all tProduit entities.
     *
     * @Route("/", name="produit_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $tProduits = $em->getRepository('AppBundle:TProduit')->findAll();

        return $this->render('tproduit/index.html.twig', array(
            'tProduits' => $tProduits,
        ));
    }

    /**
     * Creates a new tProduit entity.
     *
     * @Route("/new", name="produit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $tProduit = new Tproduit();
        $tDMoto = new TDMoto();
        $form = $this->createForm('AppBundle\Form\TProduitType', $tProduit);
        $form2 = $this->createForm('AppBundle\Form\TDMotoType', $tDMoto);
        $form->handleRequest($request);
        $form2->handleRequest($request);
        $categories= $em->getRepository('AppBundle:TCategorie')->getMotoCategories();
        if ($form->isSubmitted()) {
            $tProduit->setEstActive(true);
            $cat = $request->get('categorie');
            $category= $em->getRepository('AppBundle:TCategorie')->find($cat);
            $type= $em->getRepository('AppBundle:TTypeProduit')->find(1);
            $tProduit->setCategorie($category);
            $tProduit->setTypeProduit($type);
            $em->persist($tProduit);
            $tDMoto->setProduit($tProduit);
            $em->persist($tDMoto);

            $files = $request->files->get('imagefiles');
            foreach ($files as $pic) {
                $target = $this->getParameter("repertoire_media");
                $lienordre = $this->get('app.file_uploader')->uploadImage($pic, $target);
                $lienAbsolu = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/products/" . $lienordre;
                // Send to database
                $picture = new TMedia();
                $picture->setAlt($lienAbsolu);

                $picture->setProduit($tProduit);
                $em->persist($picture);
                $em->flush();

            }

            return $this->redirectToRoute('produit_show', array('id' => $tProduit->getId()));
        }
        return $this->render('tproduit/new.html.twig', array(
            'tProduit' => $tProduit,
            'form' => $form->createView(),
            'form2' => $form2->createView(),
            'categorie' => $categories
        ));

    }

    /**
     * Finds and displays a tProduit entity.
     *
     * @Route("/{id}", name="produit_show")
     * @Method("GET")
     */
    public function showAction(TProduit $tProduit)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $media = $em->getRepository(TMedia::class)->findBy(array(
            'produit' => $tProduit->getId(),
        ));
        $tdmoto= $em->getRepository(TDMoto::class)->findOneBy(array(
            'produit' => $tProduit->getId(),
        ));
//        dump($tdmoto);die();
        return $this->render('tproduit/show.html.twig', array(
            'tProduit' => $tProduit,
            'medias' => $media,
            'tdmoto' => $tdmoto
        ));
    }

    /**
     * Deletes a tCategorie entity.
     *
     * @Route("/type/{id}", name="personnal_produit_delete")
     * @Method("DELETE")
     */
    public function PersonaldeleteAction(Request $request, TProduit $tProduit)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();

        $tProduits = $em->getRepository('AppBundle:TProduit')->findOneById($id);

        $em->remove($tProduits);
        $em->flush();
        $tProduits = $em->getRepository('AppBundle:TProduit')->findAll();

        return $this->redirectToRoute('produit_index');

    }

    /**
     * Displays a form to edit an existing tProduit entity.
     *
     * @Route("/{id}/edit", name="produit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TProduit $tProduit)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $tDMoto= $em->getRepository(TDMoto::class)->findOneBy(array(
            'produit' => $tProduit->getId(),
        ));
        $editForm = $this->createForm('AppBundle\Form\TProduitType', $tProduit);
        $form2 = $this->createForm('AppBundle\Form\TDMotoType', $tDMoto);
        $editForm->handleRequest($request);
        $form2->handleRequest($request);
        $categories= $em->getRepository('AppBundle:TCategorie')->getMotoCategories();
        $media= $em->getRepository('AppBundle:TMedia')->findBy([
            'produit'=>$tProduit
        ]);
        if ($editForm->isSubmitted()) {
            // dump($editForm);die();
            $em = $this->getDoctrine()->getManager();
            $cat = $request->get('categorie');
            $category= $em->getRepository('AppBundle:TCategorie')->find($cat);
            $tProduit->setCategorie($category);
            $em->persist($tProduit);
            $em->persist($tDMoto);
            $em->flush();
            $files = $request->files->get('imagefiles');
            if (!empty($files)) {
                foreach ($files as $pic) {
                    $target = $this->getParameter("repertoire_media");
                    $lienordre = $this->get('app.file_uploader')->uploadImage($pic, $target);
                    $lienAbsolu = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/products/" . $lienordre;
                    // Send to database
                    $picture = new TMedia();
                    $picture->setAlt($lienAbsolu);
                    $picture->setProduit($tProduit);
                    $em->persist($picture);
                    $em->flush();

                }
            }
            return $this->redirectToRoute('produit_show', array('id' => $tProduit->getId()));
        }

        return $this->render('tproduit/edit.html.twig', array(
            'tProduit' => $tProduit,
            'edit_form' => $editForm->createView(),
            'form2' => $form2->createView(),
            'categorie' => $categories,
            'media'=>count($media)
        ));
    }

    /**
     * @Route("/change_produit/{id}", name="produit_change")
     */
    public function changeStateAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $tProduit = $em->getRepository(Tproduit::class)->find($request->get('id'));
//dump($tProduit->getEstActive());die();
        if ($tProduit->getEstActive() === true) {
            $tProduit->setEstActive(false);
            $em->persist($tProduit);
            $em->flush();
        } else {
            $tProduit->setEstActive(true);
            $em->persist($tProduit);
            $em->flush();
        }
        return $this->redirectToRoute('produit_index');
    }

}
