<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TProduitPieces;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tproduitpiece controller.
 *
 * @Route("produit_autre")
 */
class TProduitPiecesController extends Controller
{
    /**
     * Lists all tProduitPiece entities.
     *
     * @Route("/", name="produit_autre")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $tProduitPieces = $em->getRepository('AppBundle:TProduitPieces')->findAll();

        return $this->render('tproduitpieces/index.html.twig', array(
            'tProduitPieces' => $tProduitPieces,
        ));
    }

    /**
     * Creates a new tProduitPiece entity.
     *
     * @Route("/new", name="produit_autre_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $tProduitPiece = new TProduitPieces();
//        $typeId= $em->getRepository('AppBundle:TTypeProduit')->find(2);
        $categories= $em->getRepository('AppBundle:TCategorie')->getAutreCategories();
//        dump($categories);die();
        $form = $this->createForm('AppBundle\Form\TProduitPiecesType', $tProduitPiece);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
//            Add file
//            $cat= $request->get('catego');
            $file = $tProduitPiece->getImage();
            $cat = $request->get('categorie');
            $category= $em->getRepository('AppBundle:TCategorie')->find($cat);
            $target= $this->getParameter('piecedetachee_image_directory');
            $fileName = $this->generateUniqueFilename().' '.$file->guessExtension();
            $file->move(
              $target,
              $fileName
            );
            $tProduitPiece->setImage($fileName);
            $tProduitPiece->setCategorie($category);
            $tProduitPiece->setEstActive(true);
            $em->persist($tProduitPiece);
            $em->flush();

            return $this->redirectToRoute('produit_autre_show', array('id' => $tProduitPiece->getId()));
        }

        return $this->render('tproduitpieces/new.html.twig', array(
            'tProduitPiece' => $tProduitPiece,
            'form' => $form->createView(),
            'categorie' => $categories
        ));
    }

    /**
     * Finds and displays a tProduitPiece entity.
     *
     * @Route("/{id}", name="produit_autre_show")
     * @Method("GET")
     */
    public function showAction(TProduitPieces $tProduitPiece)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($tProduitPiece);

        return $this->render('tproduitpieces/show.html.twig', array(
            'tProduitPiece' => $tProduitPiece,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tProduitPiece entity.
     *
     * @Route("/{id}/edit", name="produit_autre_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TProduitPieces $tProduitPiece)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($tProduitPiece);
        $editForm = $this->createForm('AppBundle\Form\TProduitPiecesType', $tProduitPiece);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $imge= $request->get('img');
        $categories= $em->getRepository('AppBundle:TCategorie')->getAutreCategories();
        if ($editForm->isSubmitted()) {
            $cat = $request->get('categorie');
            $category= $em->getRepository('AppBundle:TCategorie')->find($cat);
            $tProduitPiece->setCategorie($category);
           if($tProduitPiece->getImage() !== null){
    //            Add file
               $file = $tProduitPiece->getImage();
               $target= $this->getParameter('piecedetachee_image_directory');
               $fileName = $this->generateUniqueFilename().' '.$file->guessExtension();
               $file->move(
                   $target,
                   $fileName
               );

               $tProduitPiece->setImage($fileName);
           }else{
               $tProduitPiece->setImage($imge);

           }
           $em->persist($tProduitPiece);
           $em->flush();

            return $this->redirectToRoute('produit_autre_show', array('id' => $tProduitPiece->getId()));
        }

        return $this->render('tproduitpieces/edit.html.twig', array(
            'tProduitPiece' => $tProduitPiece,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'categorie' => $categories
        ));
    }

    /**
     * Deletes a tProduitPiece entity.
     *
     * @Route("/{id}", name="produit_autre_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TProduitPieces $tProduitPiece)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $form = $this->createDeleteForm($tProduitPiece);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tProduitPiece);
            $em->flush();
        }

        return $this->redirectToRoute('produit_autre');
    }

    /**
     * Creates a form to delete a tProduitPiece entity.
     *
     * @param TProduitPieces $tProduitPiece The tProduitPiece entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TProduitPieces $tProduitPiece)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('produit_autre_delete', array('id' => $tProduitPiece->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
    /**
     * @Route("/change_produit_autre/{id}", name="produit_autre_change")
     */
    public function changeStateAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $tProditP = $em->getRepository(TProduitPieces::class)->find($request->get('id'));
        if ($tProditP->getEstActive() === true) {
            $tProditP->setEstActive(false);
            $em->persist($tProditP);
            $em->flush();
        } else {
            $tProditP->setEstActive(true);
            $em->persist($tProditP);
            $em->flush();
        }
        return $this->redirectToRoute('produit_autre');
    }
    /**
     * @return string
     */
    private function generateUniqueFilename(){
        return md5(uniqid());
    }

}
