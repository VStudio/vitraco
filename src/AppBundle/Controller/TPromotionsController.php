<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TMedia;
use AppBundle\Entity\TPromotions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tpromotion controller.
 *
 * @Route("promotions")
 */
class TPromotionsController extends Controller
{
    /**
     * Lists all tPromotion entities.
     *
     * @Route("/", name="promotions_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $tPromotions = $em->getRepository('AppBundle:TPromotions')->findAll();

        return $this->render('tpromotions/index.html.twig', array(
            'tPromotions' => $tPromotions,
        ));
    }

    /**
     * Creates a new tPromotion entity.
     *
     * @Route("/new", name="promotions_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $tPromotion = new Tpromotions();
        $form = $this->createForm('AppBundle\Form\TPromotionsType', $tPromotion);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted()) {
            // dump($form);
            // die();

            $tPromotion->setEstActive(true);

            $em->persist($tPromotion);
            $em->flush();
            $file = $request->files->get('imagefiles');
            if (!empty($file)) {
                $target = $this->getParameter("repertoire_media1");
                $lienordre = $this->get('app.file_uploader')->uploadImage($file, $target);
                $lienAbsolu = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/promotion/" . $lienordre;
                // Send to database
                $picture = new TMedia();
                $picture->setAlt($lienAbsolu);
                $picture->setPromotions($tPromotion);
                $em->persist($picture);
                $em->flush();

            }
            return $this->redirectToRoute('promotions_show', array('id' => $tPromotion->getId()));
        }

        return $this->render('tpromotions/new.html.twig', array(
            'tPromotion' => $tPromotion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tPromotion entity.
     *
     * @Route("/{id}", name="promotions_show")
     * @Method("GET")
     */
    public function showAction(TPromotions $tPromotion)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $media = $em->getRepository(TMedia::class)->findOneBy(array(
            'promotions' => $tPromotion->getId(),
        ));
        // dump($media);die();
        return $this->render('tpromotions/show.html.twig', array(
            'tPromotion' => $tPromotion,
            'media' => $media,
        ));
    }

    /**
     * Displays a form to edit an existing tPromotion entity.
     *
     * @Route("/{id}/edit", name="promotions_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TPromotions $tPromotion)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $editForm = $this->createForm('AppBundle\Form\TPromotionsType', $tPromotion);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $media = $em->getRepository(TMedia::class)->findOneBy(array(
            'promotions' => $tPromotion->getId(),
        ));

        if ($editForm->isSubmitted()) {

            $files = $request->files->get('imagefiles');
//                dump($files);die();
            $em->persist($tPromotion);
            $em->flush();
            $files = $request->files->get('imagefiles');
            if (!empty($files)) {

                $target = $this->getParameter("repertoire_media1");
                $lienordre = $this->get('app.file_uploader')->uploadImage($files, $target);
                $lienAbsolu = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/promotion/" . $lienordre;
                // Send to database

                if (!empty($media)) {
                    $media->setAlt($lienAbsolu);
                    $media->setPromotions($tPromotion);
                    $em->persist($media);
                } else {
                    $media = new TMedia();
                    $media->setAlt($lienAbsolu);
                    $media->setPromotions($tPromotion);
                    $em->persist($media);
                }
                $em->flush();

            }
            return $this->redirectToRoute('promotions_show', array('id' => $tPromotion->getId()));
        }

        return $this->render('tpromotions/edit.html.twig', array(
            'tPromotion' => $tPromotion,
            'edit_form' => $editForm->createView(),
            'media' => $media,
        ));
    }

    /**
     * @Route("/change_promotion/{id}", name="promotion_change")
     */
    public function changeStateAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $tPromotion = $em->getRepository(TPromotions::class)->find($request->get('id'));
//dump($tProduit->getEstActive());die();
        if ($tPromotion->getEstActive() === true) {
            $tPromotion->setEstActive(false);
            $em->persist($tPromotion);
            $em->flush();
        } else {
            $tPromotion->setEstActive(true);
            $em->persist($tPromotion);
            $em->flush();
        }
        return $this->redirectToRoute('promotions_index');
    }

}
