<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TCategorie
 *
 * @ORM\Table(name="t_categorie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TCategorieRepository")
 */
class TCategorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="estActive", type="boolean", nullable=true)
     */
    private $estActive;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=100, nullable=true)
     */
    private $libelle;
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="TTypeProduit",)
     * @ORM\JoinColumn(name="t_type_produit_id",referencedColumnName="id",nullable=false)
     *
     */
    private $typeProduit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle.
     *
     * @param string|null $libelle
     *
     * @return TCategorie
     */
    public function setLibelle($libelle = null)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string|null
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set created.
     *
     * @param \DateTime|null $created
     *
     * @return TCategorie
     */
    public function setCreated($created = null)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime|null
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set typeProduit.
     *
     * @param \AppBundle\Entity\TTypeProduit $typeProduit
     *
     * @return TCategorie
     */
    public function setTypeProduit(\AppBundle\Entity\TTypeProduit $typeProduit)
    {
        $this->typeProduit = $typeProduit;

        return $this;
    }

    /**
     * Get typeProduit.
     *
     * @return \AppBundle\Entity\TTypeProduit
     */
    public function getTypeProduit()
    {
        return $this->typeProduit;
    }

    /**
     * Set estActive.
     *
     * @param bool|null $estActive
     *
     * @return TCategorie
     */
    public function setEstActive($estActive = null)
    {
        $this->estActive = $estActive;

        return $this;
    }

    /**
     * Get estActive.
     *
     * @return bool|null
     */
    public function getEstActive()
    {
        return $this->estActive;
    }
}
