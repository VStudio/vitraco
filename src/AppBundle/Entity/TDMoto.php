<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TDMoto
 *
 * @ORM\Table(name="t_d_moto")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TDMotoRepository")
 */
class TDMoto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     *
     * @ORM\Column(name="empattement", type="integer", nullable=true)
     */
    private $empattement;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="dimension", type="string", nullable=true)
     */
    private $dimension;

    /**
     * @var string
     *
     * @ORM\Column(name="systemDeFreinage", type="string", nullable=true)
     */
    private $systemDeFreinage;

    /**
     * @var string
     *
     * @ORM\Column(name="torque", type="string", nullable=true)
     */
    private $torque;

    /**
     * @var string
     *
     * @ORM\Column(name="typeDeMoteur", type="string", nullable=true)
     */
    private $typeDeMoteur;

    /**
     * @var string
     *
     * @ORM\Column(name="fonctionnementDuFrein", type="string", nullable=true)
     */
    private $fonctionnementDuFrein;

    /**
     * @var string
     *
     * @ORM\Column(name="demarreur", type="string", nullable=true)
     */
    private $demarreur;

    /**
     * @var int
     *
     * @ORM\Column(name="puissance", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $puissance;

    /**
     * @var int
     *
     * @ORM\Column(name="capaciteReservoirEssence", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $capaciteReservoirEssence;


    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="TProduit",)
     * @ORM\JoinColumn(name="produit_id",referencedColumnName="id",nullable=false)
     *
     */
    private $produit;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set empattement.
     *
     * @param int|null $empattement
     *
     * @return TDMoto
     */
    public function setEmpattement($empattement = null)
    {
        $this->empattement = $empattement;

        return $this;
    }

    /**
     * Get empattement.
     *
     * @return int|null
     */
    public function getEmpattement()
    {
        return $this->empattement;
    }

    /**
     * Set poids.
     *
     * @param int|null $poids
     *
     * @return TDMoto
     */
    public function setPoids($poids = null)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids.
     *
     * @return int|null
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set dimension.
     *
     * @param string|null $dimension
     *
     * @return TDMoto
     */
    public function setDimension($dimension = null)
    {
        $this->dimension = $dimension;

        return $this;
    }

    /**
     * Get dimension.
     *
     * @return string|null
     */
    public function getDimension()
    {
        return $this->dimension;
    }

    /**
     * Set systemDeFreinage.
     *
     * @param string|null $systemDeFreinage
     *
     * @return TDMoto
     */
    public function setSystemDeFreinage($systemDeFreinage = null)
    {
        $this->systemDeFreinage = $systemDeFreinage;

        return $this;
    }

    /**
     * Get systemDeFreinage.
     *
     * @return string|null
     */
    public function getSystemDeFreinage()
    {
        return $this->systemDeFreinage;
    }

    /**
     * Set torque.
     *
     * @param string|null $torque
     *
     * @return TDMoto
     */
    public function setTorque($torque = null)
    {
        $this->torque = $torque;

        return $this;
    }

    /**
     * Get torque.
     *
     * @return string|null
     */
    public function getTorque()
    {
        return $this->torque;
    }

    /**
     * Set typeDeMoteur.
     *
     * @param string|null $typeDeMoteur
     *
     * @return TDMoto
     */
    public function setTypeDeMoteur($typeDeMoteur = null)
    {
        $this->typeDeMoteur = $typeDeMoteur;

        return $this;
    }

    /**
     * Get typeDeMoteur.
     *
     * @return string|null
     */
    public function getTypeDeMoteur()
    {
        return $this->typeDeMoteur;
    }

    /**
     * Set fonctionnementDuFrein.
     *
     * @param string|null $fonctionnementDuFrein
     *
     * @return TDMoto
     */
    public function setFonctionnementDuFrein($fonctionnementDuFrein = null)
    {
        $this->fonctionnementDuFrein = $fonctionnementDuFrein;

        return $this;
    }

    /**
     * Get fonctionnementDuFrein.
     *
     * @return string|null
     */
    public function getFonctionnementDuFrein()
    {
        return $this->fonctionnementDuFrein;
    }

    /**
     * Set demarreur.
     *
     * @param string|null $demarreur
     *
     * @return TDMoto
     */
    public function setDemarreur($demarreur = null)
    {
        $this->demarreur = $demarreur;

        return $this;
    }

    /**
     * Get demarreur.
     *
     * @return string|null
     */
    public function getDemarreur()
    {
        return $this->demarreur;
    }

    /**
     * Set puissance.
     *
     * @param string|null $puissance
     *
     * @return TDMoto
     */
    public function setPuissance($puissance = null)
    {
        $this->puissance = $puissance;

        return $this;
    }

    /**
     * Get puissance.
     *
     * @return string|null
     */
    public function getPuissance()
    {
        return $this->puissance;
    }

    /**
     * Set capaciteReservoirEssence.
     *
     * @param string|null $capaciteReservoirEssence
     *
     * @return TDMoto
     */
    public function setCapaciteReservoirEssence($capaciteReservoirEssence = null)
    {
        $this->capaciteReservoirEssence = $capaciteReservoirEssence;

        return $this;
    }

    /**
     * Get capaciteReservoirEssence.
     *
     * @return string|null
     */
    public function getCapaciteReservoirEssence()
    {
        return $this->capaciteReservoirEssence;
    }

    /**
     * Set produit.
     *
     * @param \AppBundle\Entity\TProduit $produit
     *
     * @return TDMoto
     */
    public function setProduit(\AppBundle\Entity\TProduit $produit)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit.
     *
     * @return \AppBundle\Entity\TProduit
     */
    public function getProduit()
    {
        return $this->produit;
    }
}
