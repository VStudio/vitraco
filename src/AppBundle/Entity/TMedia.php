<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * TMedia
 *
 * @ORM\Table(name="t_media")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TMediaRepository")
 */
class TMedia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255)
     */
    private $alt;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="TProduit")
     * @ORM\JoinColumn(referencedColumnName="id",nullable=true)
     *
     */
    private $produit;

/**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="TPromotions")
     * @ORM\JoinColumn(referencedColumnName="id",nullable=true)
     *
     */
    private $promotions;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alt.
     *
     * @param string $alt
     *
     * @return TMedia
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt.
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set produit.
     *
     * @param \AppBundle\Entity\TProduit|null $produit
     *
     * @return TMedia
     */
    public function setProduit(\AppBundle\Entity\TProduit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit.
     *
     * @return \AppBundle\Entity\TProduit|null
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set promotions.
     *
     * @param \AppBundle\Entity\TPromotions|null $promotions
     *
     * @return TMedia
     */
    public function setPromotions(\AppBundle\Entity\TPromotions $promotions = null)
    {
        $this->promotions = $promotions;

        return $this;
    }

    /**
     * Get promotions.
     *
     * @return \AppBundle\Entity\TPromotions|null
     */
    public function getPromotions()
    {
        return $this->promotions;
    }
}
