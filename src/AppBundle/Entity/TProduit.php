<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TProduit
 *
 * @ORM\Table(name="t_produit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TProduitRepository")
 */
class TProduit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="TCategorie",)
     * @ORM\JoinColumn(name="categorie_id",referencedColumnName="id",nullable=false)
     *
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="TTypeProduit",)
     * @ORM\JoinColumn(name="t_type_produit_id",referencedColumnName="id",nullable=false)
     *
     */
    private $typeProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="nomProduit", type="string", length=65)
     */
    private $nomProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer", nullable=true)
     */
    private $prix;

    /**
     * @var int
     *
     * @ORM\Column(name="vitesse", type="integer", nullable=true)
     */
    private $vitesse;

    /**
     * @var bool
     *
     * @ORM\Column(name="estActive", type="boolean", nullable=true)
     */
    private $estActive;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * @var bool
     *
     * @ORM\Column(name="estNouveau", type="boolean", nullable=true)
     */
    private $estNouveau;

   
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomProduit
     *
     * @param string $nomProduit
     *
     * @return TProduit
     */
    public function setNomProduit($nomProduit)
    {
        $this->nomProduit = $nomProduit;

        return $this;
    }

    /**
     * Get nomProduit
     *
     * @return string
     */
    public function getNomProduit()
    {
        return $this->nomProduit;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TProduit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return TProduit
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set vitesse
     *
     * @param integer $vitesse
     *
     * @return TProduit
     */
    public function setVitesse($vitesse)
    {
        $this->vitesse = $vitesse;

        return $this;
    }

    /**
     * Get vitesse
     *
     * @return integer
     */
    public function getVitesse()
    {
        return $this->vitesse;
    }

    /**
     * Set estActive
     *
     * @param boolean $estActive
     *
     * @return TProduit
     */
    public function setEstActive($estActive)
    {
        $this->estActive = $estActive;

        return $this;
    }

    /**
     * Get estActive
     *
     * @return boolean
     */
    public function getEstActive()
    {
        return $this->estActive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return TProduit
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set estNouveau
     *
     * @param boolean $estNouveau
     *
     * @return TProduit
     */
    public function setEstNouveau($estNouveau)
    {
        $this->estNouveau = $estNouveau;

        return $this;
    }

    /**
     * Get estNouveau
     *
     * @return boolean
     */
    public function getEstNouveau()
    {
        return $this->estNouveau;
    }

    /**
     * Set categorie
     *
     * @param \AppBundle\Entity\TCategorie $categorie
     *
     * @return TProduit
     */
    public function setCategorie(\AppBundle\Entity\TCategorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\TCategorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set typeProduit.
     *
     * @param \AppBundle\Entity\TTypeProduit $typeProduit
     *
     * @return TProduit
     */
    public function setTypeProduit(\AppBundle\Entity\TTypeProduit $typeProduit)
    {
        $this->typeProduit = $typeProduit;

        return $this;
    }

    /**
     * Get typeProduit.
     *
     * @return \AppBundle\Entity\TTypeProduit
     */
    public function getTypeProduit()
    {
        return $this->typeProduit;
    }
}
