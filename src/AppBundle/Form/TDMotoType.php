<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TDMotoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('empattement')
            ->add('poids')
            ->add('dimension')
            ->add('systemDeFreinage', TextType::class, array('label' => 'Système de freinage'))
            ->add('torque')
            ->add('typeDeMoteur')
            ->add('fonctionnementDuFrein')
            ->add('demarreur', TextType::class, array('label' => 'Démarreur'))
            ->add('puissance')
            ->add('capaciteReservoirEssence', TextType::class, array('label' => 'Capacité de réservoir essence'));
//            ->add('produit');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TDMoto'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_tdmoto';
    }


}
