<?php

namespace AppBundle\Form;

use AppBundle\Repository\TCategorieRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TProduitPiecesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('libelle')
//            ->add('categorie', EntityType::class, [
//                'class' => 'AppBundle\Entity\TCategorie',
//                'placeholder' => 'Chosir un catégorie',
//                'choice_label' => 'libelle',
////                'query_builder' => function(TCategorieRepository $repo) {
////                    return $repo->createQueryBuilder('c')
////                        ->field('libelle')
////                        ->where('c.typeProduit = :n') // <---
////                        ->setParameter('n',2) // <---
////                        ->orderBy('c.id', 'DESC');
////                },
//                'multiple'  => false,
//
//            ])
            ->add('image', FileType::class, array('label' => 'Image', 'data_class' => null))
            ->add('prix')
            ->add('resume',TextareaType::class);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TProduitPieces'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_tproduitpieces';
    }


}
