<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TProduitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomProduit')
            ->add('description', TextareaType::class)
            ->add('prix')
            ->add('vitesse')
            ->add('estNouveau');
//            ->add('typeProduit', EntityType::class, array(
//                'class' => 'AppBundle\Entity\TTypeProduit',
//                'choice_label' => 'libelle',
//                'placeholder' => ' Chosir le type de produit'));
            // ->add('brochure', FileType::class, array('label' => 'Image', 'data_class' => null))
//            ->add('categorie', EntityType::class, array(
//                'class' => 'AppBundle\Entity\TCategorie',
//                'choice_label' => 'libelle',
//                'placeholder' => ' Chosir une categorie',
//                'label' => 'Catégorie'
//            ));
        //->add('media',FileType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TProduit',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_tproduit';
    }

}
