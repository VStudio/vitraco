<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TPromotionsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('prix')
            ->add('model')
            ->add('libelle')
        //                ->add('created')
            // ->add('image', FileType::class, array('label' => 'Image', 'data_class' => null))
            ->add('dateFin', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
            ))
            ->add('dateDebut', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
            ))
            ->add('produit', EntityType::class, array(
                'class' => 'AppBundle\Entity\TProduit',
                'choice_label' => 'nomProduit',
                'placeholder' => 'Chosir un produit'

            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TPromotions',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_tpromotions';
    }
}
