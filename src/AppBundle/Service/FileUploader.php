<?php
namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    public function uploadImage(UploadedFile $file, $target)
    {
        if ($file->getClientOriginalExtension() != "jpg" && $file->getClientOriginalExtension() != "jpeg" && $file->getClientOriginalExtension() != "png" && $file->getClientOriginalExtension() != "JPG" && $file->getClientOriginalExtension() != "PNG") {
            $fileName = "ne_pas_une image";
        } else {
            $fileName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
            $file->move($target, $fileName);
        }

        return $fileName;
    }
}
