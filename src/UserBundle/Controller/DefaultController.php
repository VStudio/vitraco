<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('UserBundle:Security:login.html.twig');
    }
    /**
     * @Route("/promotions", name="promotions")
     */
    public function promotionlistAction()
    {
        return $this->render('adminpart/promotion.html.twig');
    }
}
