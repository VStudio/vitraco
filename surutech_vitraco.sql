-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  sql9:3306
-- Généré le :  Lun 17 Décembre 2018 à 17:02
-- Version du serveur :  10.0.32-MariaDB-0+deb8u1
-- Version de PHP :  5.4.45-0+deb7u11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `surutech_vitraco`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_categorie`
--

CREATE TABLE IF NOT EXISTS `t_categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `t_type_produit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C2178FC25D041C66` (`t_type_produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Contenu de la table `t_categorie`
--

INSERT INTO `t_categorie` (`id`, `libelle`, `created`, `t_type_produit_id`) VALUES
(1, 'SY150', '2018-10-10 09:54:25', 1),
(2, 'SY110', '2018-10-10 09:54:38', 1),
(3, 'SY125', '2018-10-10 12:00:28', 1),
(4, 'LIFAN', '2018-10-10 13:08:14', 1),
(5, 'MOTEUR C110', '2018-10-11 19:11:53', 3),
(6, 'CG125', '2018-10-12 20:21:38', 3),
(7, 'Autre Pieces', '2018-11-13 09:24:01', 3),
(8, 'SY110-12-22 SY50-12', '2018-11-13 09:27:27', 3),
(9, 'UFB150-UFB200', '2018-11-13 09:28:03', 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_d_moto`
--

CREATE TABLE IF NOT EXISTS `t_d_moto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produit_id` int(11) NOT NULL,
  `empattement` int(11) DEFAULT NULL,
  `poids` int(11) DEFAULT NULL,
  `dimension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `systemDeFreinage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `torque` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeDeMoteur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fonctionnementDuFrein` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `demarreur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `puissance` decimal(10,2) DEFAULT NULL,
  `capaciteReservoirEssence` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AD1AAFF3F347EFB` (`produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Contenu de la table `t_d_moto`
--

INSERT INTO `t_d_moto` (`id`, `produit_id`, `empattement`, `poids`, `dimension`, `systemDeFreinage`, `torque`, `typeDeMoteur`, `fonctionnementDuFrein`, `demarreur`, `puissance`, `capaciteReservoirEssence`) VALUES
(1, 2, 1306, 130, '2050X800X1115', 'Foot/Chain', '10.0', '162FMJ', 'Drum/Drum', 'Electric start/ Kick start', 9.00, 12.00),
(2, 1, 1340, 120, '2150X800X1120', 'Foot/Chain', '10.0', '162FMJ', 'Drum/Drum', 'Electric start/Kick start', 8.50, 12.00),
(4, 3, 1310, 213, '2050X800X1115', 'Foot/Chain', '10.0', '162FMJ', 'Drum/Drum', 'Electric start/ Kick start', 9.00, 12.00),
(5, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 5, 1320, 107, '1995X796X1005', 'Le style plat / le style de tambour', '124', '157FMI', 'Freinage a la main / freinage a pied', 'Kick Starter', 7.00, 12.00),
(7, 6, 1220, 93, '1945X700X1060', 'Le style plat / le style de tambour', '5.0', '152FMH', 'Freinage a la main / freinage a pied', 'Démarreur de coup de pied', 107.00, 20.00),
(8, 7, 1280, 136, '1960X810X114', 'Le style plat / le style de tambour', NULL, '162FMJ', 'Freinage a la main / freinage a pied', 'Démarreur électrique / Démarreur à coup de pied', 8.50, 14.50),
(9, 8, 1280, 125, '1930X735X1067', 'Le style plat / le style de tambour', '10', '162FMJ', 'Freinage a la main / freinage a pied', 'Démarreur électrique / Démarreur à coup de pied', 8.50, 14.50),
(10, 9, 1295, 118, '2015X835X1145', 'Le style plat / le style de tambour', '10', '162FMJ', 'Freinage a la main / freinage a pied', 'Démarreur électrique / Démarreur à coup de pied', 8.50, 9.70),
(11, 10, 1220, 104, '1910X690X1020', 'Le style plat / le style de tambour', '7.0', '152FMJ', 'Freinage a la main / freinage a pied', 'Démarreur électrique / Démarreur à coup de pied', 5.00, 4.00);

-- --------------------------------------------------------

--
-- Structure de la table `t_media`
--

CREATE TABLE IF NOT EXISTS `t_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produit_id` int(11) DEFAULT NULL,
  `promotions_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D49DC775F347EFB` (`produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Contenu de la table `t_media`
--

INSERT INTO `t_media` (`id`, `alt`, `produit_id`, `promotions_id`) VALUES
(1, 'http://vitraco.fr/web/uploads/products/8ca31c332ceee2f04a1daf5e9986339f.jpg', 1, NULL),
(2, 'http://vitraco.fr/web/uploads/products/58b79c2f8e4376505c00c42827466698.jpg', 1, NULL),
(3, 'http://vitraco.fr/web/uploads/products/d753042ee973123c8c1c65d5aebecfd5.jpg', 1, NULL),
(4, 'http://vitraco.fr/web/uploads/products/c3c1d7db547bf2383ad2ffa2835b82a9.jpg', 3, NULL),
(5, 'http://vitraco.fr/uploads/products/2cd578d47cf62e6a3d5cc86297d1077d.jpg', 4, NULL),
(6, 'http://vitraco.fr/uploads/promotion/615de6392d99f8856d93e5abc231a114.jpg', NULL, 1),
(7, 'http://vitraco.fr/uploads/products/e5154abbd69c42fe0e3985a45e056c29.jpg', 5, NULL),
(8, 'http://vitraco.fr/uploads/products/bdacdbe3c1d4e08b6beab8127dd6bd82.jpg', 6, NULL),
(9, 'http://vitraco.fr/uploads/products/f772e93e987b96c52b22c8d4d74532e4.jpg', 7, NULL),
(10, 'http://vitraco.fr/uploads/products/2dc2b74f7487a23a5669fa191ba07c5f.jpg', 8, NULL),
(11, 'http://vitraco.fr/uploads/products/78246d9688c119e05cda259fafc84222.jpg', 9, NULL),
(12, 'http://vitraco.fr/uploads/products/a8853689d9bdd52701d13502fb70a1ac.jpg', 10, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_produit`
--

CREATE TABLE IF NOT EXISTS `t_produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_id` int(11) NOT NULL,
  `nomProduit` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `vitesse` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `estNouveau` tinyint(1) DEFAULT NULL,
  `estActive` tinyint(1) DEFAULT NULL,
  `t_type_produit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_73895444BCF5E72D` (`categorie_id`),
  KEY `IDX_738954445D041C66` (`t_type_produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `t_produit`
--

INSERT INTO `t_produit` (`id`, `categorie_id`, `nomProduit`, `description`, `prix`, `vitesse`, `created`, `estNouveau`, `estActive`, `t_type_produit_id`) VALUES
(1, 1, 'SY150-18HH', 'SY150-18H', 450000, 150, '2018-10-10 12:30:35', 1, 1, 1),
(3, 1, 'SY150-18', 'SY150-18', NULL, 123, '2018-10-10 13:18:18', 1, 1, 1),
(4, 3, 'SANYA SY125-26', 'SY125-26', 385000, 150, '2018-10-10 13:33:22', 1, 1, 1),
(5, 3, 'SY125 -6', 'La serie de SY125-6 est un type qui lanc6 fortem ent par l''entreprise\r\nSANYA, l''apparence par laquelle elle est entierement distinguee, par la mode\r\nqui est beaucoup plus achetee par les gens qui aiment cette motocyclette qui\r\nest devenue le directeur de ', NULL, 90, '2018-10-11 14:18:39', 0, 1, 1),
(6, 2, 'SY110-11', 'SY110-11', NULL, 80, '2018-10-11 16:51:27', 0, 1, 1),
(7, 1, 'SY150-36B', 'SY150-36B', NULL, 90, '2018-10-11 16:55:25', 0, 1, 1),
(8, 1, 'SY150-9HK', 'SY150-9HK', NULL, 149, '2018-10-11 16:59:12', 0, 1, 1),
(9, 1, 'SY150-27', 'SY150-27', NULL, 149, '2018-10-11 17:02:55', 0, 1, 1),
(10, 2, 'SY110-12', 'SY110-12', NULL, 107, '2018-10-11 17:18:43', 0, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `t_produit_pieces`
--

CREATE TABLE IF NOT EXISTS `t_produit_pieces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estActive` tinyint(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `prix` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A23955EB497DD634` (`categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=104 ;

--
-- Contenu de la table `t_produit_pieces`
--

INSERT INTO `t_produit_pieces` (`id`, `categorie`, `libelle`, `image`, `resume`, `estActive`, `created`, `prix`) VALUES
(1, 5, 'Tuyau d''entrée d''air secondaire', 'e0fd3705abd5ff36d978053bc5d00ed1 png', 'Tuyau d''entrée d''air secondaire', 0, '2018-10-12 20:23:47', 2000),
(2, 7, 'Joint, pompe à huile', '04f3bf704f7e278092e871a9dcef2456 jpeg', 'Joint, pompe à huile', 1, '2018-11-15 06:24:10', NULL),
(3, 7, 'Douille, arbre, pignon de pompe à huile', '8a3fa5777dca8246d12bd8bc9412f904 jpeg', 'Douille, arbre, pignon de pompe à huile', 1, '2018-11-15 14:01:03', NULL),
(4, 7, 'Pignon, pompe', '62754a1dd9df1d60c20fc0a29c55d3f2 jpeg', 'Pignon, pompe', 1, '2018-11-15 14:01:40', NULL),
(5, 7, 'Pompe à huile', '42b6014d0aa8917784d1af14bcc9d6d7 jpeg', 'Pompe à huile', 1, '2018-11-15 15:03:25', NULL),
(6, 7, 'Compteur d''arbre, démarreur à pied', '30f1d54501761ff78f09985eea41652b jpeg', 'Compteur d''arbre, démarreur à pied', 1, '2018-11-15 15:05:57', NULL),
(7, 7, 'Broussaille', 'e7d22f55fce603b836f02dc73a72be32 jpeg', 'Broussaille', 1, '2018-11-15 15:11:10', NULL),
(8, 7, 'Démarrage en chaîne', '7b30e9ad873386fe7c3e8c48ef42c62f jpeg', 'Démarrage en chaîne', 1, '2018-11-15 15:13:00', NULL),
(9, 7, 'Génératrice', 'fad9fd1ccef07650afa5e359cd6a2ef5 jpeg', 'Génératrice', 1, '2018-11-15 15:15:22', NULL),
(10, 7, 'Ensemble de pignons, démarrage', 'c9518e9a67a7075c5f0f9bf382946e8b jpeg', 'Ensemble de pignons, démarrage', 1, '2018-11-15 15:17:09', NULL),
(11, 7, 'Capot, barrière', '594f398bd022571340358d8f3014c91d jpeg', 'Capot, barrière', 1, '2018-11-15 15:19:47', NULL),
(12, 7, 'Rotateur comp', 'ca6db6efa60bf839d7f94d945f23400e jpeg', 'Rotateur comp', 1, '2018-11-15 15:21:21', NULL),
(13, 7, 'stator comp', '056bf8ce0c362aec9d0499da44f77828 jpeg', 'stator comp', 1, '2018-11-15 15:51:37', NULL),
(14, 7, 'Allumeur', 'd9f7f36e3289714155d518ac8b102723 jpeg', 'Allumeur', 1, '2018-11-15 15:52:51', NULL),
(15, 7, 'Boulon', 'fad167eb6e6036354f9ea77453346abe jpeg', 'Boulon', 1, '2018-11-15 15:54:50', NULL),
(16, 7, 'Bras tendeur de chaîne', '223cd2e6e57754787b32d600bf46fae3 jpeg', 'Bras tendeur de chaîne', 1, '2018-11-15 15:56:21', NULL),
(17, 7, 'Écrou d''étanchéité', 'bf753593f33820bba7b283bf4aa8caed jpeg', 'Écrou d''étanchéité', 1, '2018-11-15 15:57:23', NULL),
(18, 7, 'Pince de piston', '81fa790acc0771a334979882e89cad62 jpeg', 'Pince de piston', 1, '2018-11-15 15:59:08', NULL),
(19, 7, 'Localiser le levier de vitesse de la cheville', '431c98fa6e738f15ecab483aa6887337 jpeg', 'Localiser le levier de vitesse de la cheville', 1, '2018-11-15 16:00:37', NULL),
(20, 7, 'Palier 6001', 'e72d371435ebf8fe277009370e8a2924 jpeg', 'Palier 6001', 1, '2018-11-15 16:03:46', NULL),
(21, 7, 'Palier 6201', '80952d9d4d4b747cba4d65d7031e3015 jpeg', 'Palier 6201', 1, '2018-11-15 16:04:43', NULL),
(22, 7, 'Palier 6203', '27a426ccfedc1948985617cc49b30d99 jpeg', 'Palier 6203', 1, '2018-11-15 16:05:34', NULL),
(23, 7, 'Aiguille de roulement', '37c5aca8f4b789a467bd50d7bf8dd969 jpeg', 'Aiguille de roulement', 1, '2018-11-15 16:06:37', NULL),
(24, 7, 'Vanne d''étanchéité', 'a1ffd6295295ffaae43bec010d9e17d0 jpeg', 'Vanne d''étanchéité', 1, '2018-11-15 16:40:52', NULL),
(25, 7, 'Clanper, siphon de tube de respiration', 'b43a71531142eb0e5a8243ed53c96bcf jpeg', 'Clanper, siphon de tube de respiration', 1, '2018-11-15 16:47:40', NULL),
(26, 7, 'Joint de carter', 'b33dc153ac1fb00d771b4eb5937f3716 jpeg', 'Joint de carter', 1, '2018-11-15 16:51:47', NULL),
(27, 7, 'Carter, comp droite', 'ec5dabd3dd7fa685f90d12f7f088ca9e jpeg', 'Carter, comp droite', 1, '2018-11-15 16:53:22', NULL),
(28, 7, 'Écran, filtre à huile', 'e387e31f385718290a61f16534f29e12 jpeg', 'Écran, filtre à huile', 1, '2018-11-15 16:54:48', NULL),
(29, 7, 'Assiette, huile séparée', 'fee421530ee50df2e825411c180d5d61 jpeg', 'Assiette, huile séparée', 1, '2018-11-15 16:56:33', NULL),
(30, 7, 'Protector assy, chaîne de départ', '11898ecc3783b4cc805eb6c46eb8d040 jpeg', 'Protector assy, chaîne de départ', 1, '2018-11-15 16:58:48', NULL),
(31, 7, 'Assiette de plaque, guide de chaîne de départ', 'e1575a3f01ba92c6b9b133b869d533ef jpeg', 'Assiette de plaque, guide de chaîne de départ', 1, '2018-11-15 17:00:31', NULL),
(32, 7, 'Plateau, réglage du pignon de départ', '12218e3701c305f454d150f116216b58 jpeg', 'Plateau, réglage du pignon de départ', 1, '2018-11-15 17:02:10', NULL),
(33, 7, 'Composant de carter gauche', '6f3316337d704dc4f204c25623138105 jpeg', 'Composant de carter gauche', 1, '2018-11-15 17:04:37', NULL),
(34, 7, 'Affichage de vitesse', '7afb71bc58e0425170881f8ff79e1097 jpeg', 'Affichage de vitesse', 1, '2018-11-15 17:05:58', NULL),
(35, 5, 'Piston', 'ecafc656b0a8c140261bd4849cbaebf1 jpeg', 'Piston', 1, '2018-11-15 17:07:40', NULL),
(36, 7, 'Piston de goupille', 'a0db71143ce447880464f629bfa9b232 jpeg', 'Piston de goupille', 1, '2018-11-15 17:09:19', NULL),
(37, 7, 'Segments de piston', '3c34af839e06fddfa97718d25e49dc59 jpeg', 'Segments de piston', 1, '2018-11-15 17:11:15', NULL),
(38, 7, 'Comp, vilebrequin', '622409ee42f9eb57b8eb5d3b312efcb0 jpeg', 'Comp, vilebrequin', 1, '2018-11-15 17:14:42', NULL),
(39, 7, 'Pignon, entraînement', 'dd652be3e1a59db1ca4bc40829d6dedb jpeg', 'Pignon, entraînement', 1, '2018-11-15 17:16:36', NULL),
(40, 7, 'Plaque de fixation', '8724a954ce4ffd0505e1bc7d48dea7c6 jpeg', 'Plaque de fixation', 1, '2018-11-15 17:18:30', NULL),
(41, 7, 'Printemps, bouchon retour', '722372887c063ff8f6b5993015ff6fb7 jpeg', 'Printemps, bouchon retour', 1, '2018-11-15 17:28:01', NULL),
(42, 7, 'Arbre principal, Comp', '72c5be97edf12e12761d12587da91211 jpeg', 'Arbre principal, Comp', 1, '2018-11-15 17:29:29', NULL),
(43, 7, 'Arbre intermédiaire, Comp', '8aedfe06f094844956c921dc0dda0d99 jpeg', 'Arbre intermédiaire, Comp', 1, '2018-11-15 17:30:44', NULL),
(44, 7, 'Assy, tambour de changement de vitesse', '952207b41e494412572626b855270f05 jpeg', 'Assy, tambour de changement de vitesse', 1, '2018-11-15 17:32:09', NULL),
(45, 7, 'Stopper assy, changement de vitesse', '682c5cd608d3120826cf00c4b6d6d3d3 jpeg', 'Stopper assy, changement de vitesse', 1, '2018-11-15 17:35:18', NULL),
(46, 5, 'Changement de vitesse', '25fa19ee3842946e0281bfdd5c7d603c jpeg', 'Changement de vitesse', 1, '2018-11-15 17:36:16', NULL),
(47, 7, 'Embrayage', '4f677d933d5b6e2ba9a0927abb53f219 jpeg', 'Embrayage', 1, '2018-11-15 17:37:17', NULL),
(48, 7, 'Niveau assy,  embrayage', '11722b98ba767cc74b4707928bffc6fd jpeg', 'Niveau assy,  embrayage', 1, '2018-11-15 17:39:13', NULL),
(49, 7, 'O bagues', '15e954bc327cb43f9018b2ef8c89f373 jpeg', 'O bagues', 1, '2018-11-15 17:42:22', NULL),
(50, 7, 'Joint rectangulaire', '23103f02e2eacb0eddd4526b65389749 jpeg', 'Joint rectangulaire', 1, '2018-11-15 17:43:26', NULL),
(51, 7, 'Joint rectangulaire', 'ae6f54c370317400bce384a525bc2c21 jpeg', 'Joint rectangulaire', 1, '2018-11-15 17:44:20', NULL),
(52, 7, 'Joint, changement de vitesse', '8cbd57b81646f8901e6539b234abefb3 jpeg', 'Joint, changement de vitesse', 1, '2018-11-15 17:45:30', NULL),
(53, 7, 'Joint, essieu de départ', '53821c1a34e74d230915b5435544a4ef jpeg', 'Joint, essieu de départ', 1, '2018-11-15 17:48:47', NULL),
(54, 7, 'Joint rectangulaire', '74cc1d11443879abb489a0b490491fa3 jpeg', 'Joint rectangulaire', 1, '2018-11-15 17:50:47', NULL),
(55, 7, 'Joint d''étanchéité, transmission de contre-arbre', '347a2bf4eac472edfef5ffe9978bf79c jpeg', 'Joint d''étanchéité, transmission de contre-arbre', 1, '2018-11-15 17:52:27', NULL),
(56, 7, 'Joint', '4e8732e6927f98bd6a2c54857fbd9fb6 jpeg', 'Joint', 1, '2018-11-15 17:53:17', NULL),
(57, 7, 'O bagues', 'c1a9173be13226c0de50c54b5c1b4381 jpeg', 'O bagues', 1, '2018-11-15 17:56:00', NULL),
(58, 7, 'Joint', '0e9314a56ad2fab82a1ccd846bb477e5 jpeg', 'Joint', 1, '2018-11-15 17:56:31', NULL),
(59, 7, 'O bagues', 'c2d400d92af225cbff638ee97fd82677 jpeg', 'O bagues', 1, '2018-11-15 17:57:36', NULL),
(60, 6, 'Culasse', '5ab641e01f95e042a13a78d7ab082876 jpeg', 'Culasse', 1, '2018-11-15 17:59:15', NULL),
(61, 6, 'O Bagues,Tuyau d''échappement', '81de70f510b107baf0c6eab84dcc1187 jpeg', 'O Bagues,Tuyau d''échappement', 1, '2018-11-15 18:00:49', NULL),
(62, 6, 'Tuyau d''entrée d''air secondaire', 'cae321c720a5b6a5537d5e99706d3fca jpeg', 'Tuyau d''entrée d''air secondaire', 1, '2018-11-15 18:01:59', NULL),
(63, 7, 'Frein à disque arrière', 'e79d50bfb58b896a29d20d2cee714632 jpeg', 'Frein à disque arrière', 1, '2018-11-15 18:07:45', NULL),
(64, 7, 'Frein à disque arrière', '30ae7f48623e60a74a548097f85711f6 jpeg', 'Frein à disque arrière', 1, '2018-11-15 18:08:55', NULL),
(65, 7, 'Axe de roue avant', '23c338f7e17258a6f0566b4b15b0fa36 jpeg', 'Axe de roue avant', 1, '2018-11-15 18:09:52', NULL),
(66, 7, 'Bague d''essieu de roue avant', '85244b1357b7ffd30cab441244eb5d9f jpeg', 'Bague d''essieu de roue avant', 1, '2018-11-15 18:12:07', NULL),
(67, 7, 'Roue avant', '8826f9ff3cc4c51763ffd2344864d007 jpeg', 'Roue avant', 1, '2018-11-15 19:12:35', NULL),
(68, 7, 'Assy pneu avant', 'de004cb2cb0ce33312fb304756063c1e jpeg', 'Assy pneu avant', 1, '2018-11-15 19:14:21', NULL),
(69, 7, 'Compteur', '29d1b9809715d856fcd6ae311ab4f4b7 jpeg', 'Compteur', 1, '2018-11-15 19:15:44', NULL),
(70, 7, 'Ensemble de frein à tambour avant', '202c6ac56a86902217507819ec8396be jpeg', 'Ensemble de frein à tambour avant', 1, '2018-11-15 19:17:53', NULL),
(71, 7, 'Frein à disque avant', '8afa4ae8f5a5b1665ff1d01d2e10c8bb jpeg', 'Frein à disque avant', 1, '2018-11-15 19:20:06', NULL),
(72, 7, 'Comp. Frein à disque avant', '676d22ac4b015fc7ab519478472c3de0 jpeg', 'Comp. Frein à disque avant', 1, '2018-11-15 19:21:25', NULL),
(73, 7, 'Support principal printemps', 'c0f981cbb2e2f01f88dcb1f4bf966fe2 jpeg', 'Support principal printemps', 1, '2018-11-15 19:22:48', NULL),
(74, 7, 'Ressort de béquille', '8d29905a08287905565ef841841e1d6f jpeg', 'Ressort de béquille', 1, '2018-11-15 19:23:42', NULL),
(75, 7, 'Boîte à outils', 'a05f8eff1f7d29ce41ea1b09ae369211 jpeg', 'Boîte à outils', 1, '2018-11-15 19:25:01', NULL),
(76, 7, 'Support de panneau avant', '4658716369fd9ab84205d6860b651dd3 jpeg', 'Support de panneau avant', 1, '2018-11-15 19:26:57', NULL),
(77, 7, 'Cadre', '12a364e12d902df8a75150a58c5b807b jpeg', 'Cadre', 1, '2018-11-15 19:28:07', NULL),
(78, 7, 'Petit transporteur', '0d2dd51861800b3342b7d8de1e6cc84a jpeg', 'Petit transporteur', 1, '2018-11-15 19:30:10', NULL),
(79, 5, 'Grand repose-pied arrière', '56694f7bdee64af56323c969d5e60385 jpeg', 'Grand repose-pied arrière', 1, '2018-11-15 19:31:36', NULL),
(80, 7, 'Grand repose-pied arrière R', '75459ad8a7af576a73936e0af6fc02cb jpeg', 'Grand repose-pied arrière R', 1, '2018-11-15 19:32:12', NULL),
(81, 7, 'Crochet à ressort, support principal', '0f67f2110a226728abfe3184da9bd60c jpeg', 'Crochet à ressort, support principal', 1, '2018-11-15 19:33:14', NULL),
(82, 7, 'Support principal Essieu', 'a581014cf7f10b27c58c49a7b190f409 jpeg', 'Support principal Essieu', 1, '2018-11-15 19:34:21', NULL),
(83, 7, 'Support latéral', '0d98e67b4d8cc75c8bc3e5038d122af0 jpeg', 'Support latéral', 1, '2018-11-15 19:35:26', NULL),
(84, 7, 'Barre à pédale', '96ba395d81a1747d8322f24abd8ef56b jpeg', 'Barre à pédale', 1, '2018-11-15 19:36:56', NULL),
(85, 7, 'Caoutchouc avant', '6a04ab1cb70051dce35e5a3aa473c832 jpeg', 'Caoutchouc avant', 1, '2018-11-15 19:38:11', NULL),
(86, 7, 'Douille de marche arrière', 'cff5029a0745ff4c2e5b8ccf152a6f85 jpeg', 'Douille de marche arrière', 1, '2018-11-15 19:39:14', NULL),
(87, 7, 'Alliage sriangle plaque,D', 'bf5472e15f9728b973abb21a047b5b68 jpeg', 'Alliage sriangle plaque,D', 1, '2018-11-15 19:42:00', NULL),
(88, 7, 'Alliage sriangle plaque,G', 'fffd81f491bd3f6fabbc88436beec2a8 jpeg', 'Alliage sriangle plaque,G', 1, '2018-11-15 19:42:43', NULL),
(89, 7, 'Assy principale', '9e5688edd46e8c756af3d1fcba642b6e jpeg', 'Assy principale', 1, '2018-11-15 19:43:59', NULL),
(90, 7, 'Bascule', '39789d21f6f363bf2c52d36b1cf38216 jpeg', 'Bascule', 1, '2018-11-15 19:44:55', NULL),
(91, 7, 'Manchon d''essieu gauche intérieur', '9d3baa42a27dd33748011898f047c379 jpeg', 'Manchon d''essieu gauche intérieur', 1, '2018-11-15 19:47:18', NULL),
(92, 7, 'Balançoire arrière', 'e54564e6e0acbe73ab734403f6563b35 jpeg', 'Balançoire arrière', 1, '2018-11-15 19:48:18', NULL),
(93, 7, 'Essieu arrière pivotant', 'e35195b6a12fbdcaf426bab5260f03ed jpeg', 'Essieu arrière pivotant', 1, '2018-11-15 19:49:14', NULL),
(94, 7, 'Absorbeur arrière', '03432c2d69aa8b394429b04bcd782ad2 jpeg', 'Absorbeur arrière', 1, '2018-11-15 19:50:01', NULL),
(95, 7, 'Caoutchouc Grip Gauche', '3371cc8b2d01bf914991a0c37ca18423 jpeg', 'Caoutchouc Grip Gauche', 1, '2018-11-15 19:52:40', NULL),
(96, 7, 'Barre de direction', '273d1cc6944f70e446f777364e668d75 jpeg', 'Barre de direction', 1, '2018-11-15 19:53:53', NULL),
(97, 7, 'Poignée d''accélérateur', '6261384bd60c8fcaa4aba845ff092d99 jpeg', 'Poignée d''accélérateur', 1, '2018-11-15 19:54:45', NULL),
(98, 7, 'Changer d''assy', 'decf30b8d71525158f70d4bbbc7a124f jpeg', 'Changer d''assy', 1, '2018-11-15 19:56:05', NULL),
(99, 7, 'Balancier', '0b6a85e0e69ba9cc4a39bc1f3b5268de jpeg', 'Balancier', 1, '2018-11-15 19:57:21', NULL),
(100, 7, 'Balancier', '4ee0340a43f1de70fbac0f3c994a0bc8 jpeg', 'Balancier', 1, '2018-11-15 19:57:24', NULL),
(101, 7, 'Partie avant du garde-boue', '2eeb95eabe48109b4f5718389e8ce1fe jpeg', 'Partie avant du garde-boue', 1, '2018-11-15 19:58:43', NULL),
(102, 7, 'Partie arrière du garde-boue', 'abad30cbb823e4a308e5af05ff9b2fb0 jpeg', 'Partie arrière du garde-boue', 1, '2018-11-15 19:59:36', NULL),
(103, 7, 'Plaque publicitaire avant', 'b9d232f3e446afca85bbdf749c71bace jpeg', 'Plaque publicitaire avant', 1, '2018-11-15 22:48:45', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_promotions`
--

CREATE TABLE IF NOT EXISTS `t_promotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produit_id` int(11) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `estActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BE4E0D44F347EFB` (`produit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `t_promotions`
--

INSERT INTO `t_promotions` (`id`, `produit_id`, `prix`, `model`, `created`, `libelle`, `date_debut`, `date_fin`, `estActive`) VALUES
(1, 4, 385000, 'SY152-26', '2018-10-10 13:39:50', 'Promotion 1', '2018-10-09', '2022-10-05', 1);

-- --------------------------------------------------------

--
-- Structure de la table `t_type_produit`
--

CREATE TABLE IF NOT EXISTS `t_type_produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `t_type_produit`
--

INSERT INTO `t_type_produit` (`id`, `libelle`, `created`) VALUES
(1, 'Motos', '2018-10-10 10:53:40'),
(2, 'Lubrifiants', '2018-10-10 10:53:52'),
(3, 'Piece Detachees', '2018-10-10 10:54:10');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'paul', 'paul', 'myclash401@gmail.com', 'myclash401@gmail.com', 1, NULL, '$2y$13$rUW/0tr0p4t2aaXTTD2yt.Ryquu803LaJgTeQsik4rTF3E6AcCa6K', '2018-12-10 18:40:09', NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_categorie`
--
ALTER TABLE `t_categorie`
  ADD CONSTRAINT `FK_C2178FC25D041C66` FOREIGN KEY (`t_type_produit_id`) REFERENCES `t_type_produit` (`id`);

--
-- Contraintes pour la table `t_d_moto`
--
ALTER TABLE `t_d_moto`
  ADD CONSTRAINT `FK_AD1AAFF3F347EFB` FOREIGN KEY (`produit_id`) REFERENCES `t_produit` (`id`);

--
-- Contraintes pour la table `t_media`
--
ALTER TABLE `t_media`
  ADD CONSTRAINT `FK_D49DC775F347EFB` FOREIGN KEY (`produit_id`) REFERENCES `t_produit` (`id`);

--
-- Contraintes pour la table `t_produit`
--
ALTER TABLE `t_produit`
  ADD CONSTRAINT `FK_738954445D041C66` FOREIGN KEY (`t_type_produit_id`) REFERENCES `t_type_produit` (`id`),
  ADD CONSTRAINT `FK_73895444BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `t_categorie` (`id`);

--
-- Contraintes pour la table `t_produit_pieces`
--
ALTER TABLE `t_produit_pieces`
  ADD CONSTRAINT `FK_A23955EB497DD634` FOREIGN KEY (`categorie`) REFERENCES `t_categorie` (`id`);

--
-- Contraintes pour la table `t_promotions`
--
ALTER TABLE `t_promotions`
  ADD CONSTRAINT `FK_BE4E0D44F347EFB` FOREIGN KEY (`produit_id`) REFERENCES `t_produit` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
