var ajax;
ajax = {
    loadmorebike: function () {
        var page = $('#pagenumber').val();
        var libelle = $('#libelle').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMoreBikes'),
            data: {
                page: page,
                libelle: libelle,
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#firstbatch').append(data);
            }
        });
    },

    loadMoreMewBikes: function () {
        var page = $('#pagenumber').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMoreMewBikes'),
            data: {
                page: page,
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#firstbatch').append(data);
            }
        });
    },
    loadmorebike2: function () {
        var page = $('#pagenumber').val();
        var libelle = $('#libelle').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMoreBikes2'),
            data: {
                page: page,
                libelle: libelle,
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#firstbatch').append(data);
            }
        });
    },
    loadmoremsglist: function () {
        var page = $('#pagenumber').val();
        var libelle = $('#libelle').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMoreMsg'),
            data: {
                page: page,
                libelle: libelle,
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#firstbatch').append(data);
            }
        });
    },

    loadmorePiece: function () {
        var page = $('#pagenumber').val();
        var libelle = $('#libelle').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMorePiece'),
            data: {
                page: page,
                libelle: libelle,
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#firstbatch').append(data);
            }
        });
    },
    loadMoreLub: function () {
        var page = $('#pagenumber').val();
        var libelle = $('#libelle').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMoreLub'),
            data: {
                page: page,
                libelle: libelle,
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#firstbatch').append(data);
            }
        });
    },
    loadMoreLubCat: function () {
        var page = $('#pagenumber').val();
        var libelle = $('#libelle').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMoreLubCat'),
            data: {
                page: page,
                libelle: libelle,
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#firstbatch').append(data);
            }
        });
    },
};
// });